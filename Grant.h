/********************************
 *
 * PMI implementation of Grant impact ionization model
 *
 * Written by Taylor Shin
 *
 */

#ifndef _PMI_GRANT_MODEL_H_
#define _PMI_GRANT_MODEL_H_
 
#include <cmath>

#include "PMI.h"

/*
class PMI_Avalanche : public PMI_Vertex_Interface {

private:
  const PMI_AvalancheDrivingForce drivingForce;
  
public:

  PMI_Avalanche (const PMI_Environment& env,
    const PMI_AvalancheDrivingForce force);
  virtual ~PMI_Avalanche();
  
  PMI_AvalancheDrivingForce AvalancheDrivingForce () const
  { return drivingForce; }
  
  virtual void Compute_alpha (const double F) = 0;
};

typedef PMI_Avalanche* new_PMI_Avalanche_func(
  const PMI_Environment& env, const PMI_AvalancheDrivingForce force);
extern "C" 
new_PMI_Avalanche_func new_PMI_e_Avalanche;
extern "C" 
new_PMI_Avalanche_func new_PMI_h_Avalanche;
*/

#endif /* Include guard */
