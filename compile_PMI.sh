#!/bin/sh

# Current directory
CWD="$(pwd -P)"

# Compile options (cmi, not gcc)
CMI_OPTS="-O --openmp"

# Some useful functions
die() {
    printf 'ERROR: %s\n' "$1"
    exit 1
}

while :; do
    case "$1" in
        "--clean")
            rm -rfv ./*.o ./*.so.*
            printf 'Cleaned up PMI libraries\n'
            exit 0
            ;;
        -v|--verbose)
            printf 'Verbose mode\n'
            CMI_OPTS="-O --openmp -v"
            shift
            ;;
        *)
            break
            ;;
    esac
done

# Checking if cmi command can be found.
CMI_CMD="$(command -v cmi)"
if [ ! -x "$CMI_CMD" ]; then
  die "cmi command cannot be found!"
fi

# Select gcc type here
# it can be 'system' 'uds'
GCC_TYPE='uds'
UDS_GCC4Sen_PATH="$HOME/.local/.opt/gcc4-sentaurus/bin"
SYS_GCC='/usr/bin'
case "$GCC_TYPE" in
    'uds')

        if [ -x "$(command -v $UDS_GCC4Sen_PATH/gcc)" ]; then
            printf 'Using %s as compiler.\n' "$UDS_GCC4Sen_PATH/gcc"
            GCC_PATH="$UDS_GCC4Sen_PATH"
        else
            printf 'UDS provided gcc cannot be found! Reverting back to system gcc\n'
            GCC_PATH="$SYS_GCC"
        fi
        ;;
    *)
        printf 'Just using system GCC\n'
        GCC_PATH="$SYS_GCC"
  	    ;;
esac


compile_PMI_src() {
    if [ -f ${1%.*}.o ]; then
        printf 'Compiled object exists for %s\n' "$1"
        return
    fi
    printf 'Compiling %s\n' "$1"
    PATH="$GCC_PATH:$PATH"
    compile_cmd="$(echo "$CMI_CMD" "$CMI_OPTS" "$CWD/$1")"
    $compile_cmd || die "Compilation failed!!"
} # compile_PMI_src() 

# Let's run the compilation
# rm -rf "$CWD/*.o" "$CWD/*.so.*"
for item in "."/*.C; do
    [ -f "$item" ] && compile_PMI_src "$item"
done
for item in "."/*.cpp; do
    [ -f "$item" ] && compile_PMI_src "$item"
done
for item in "."/*.cc; do
    [ -f "$item" ] && compile_PMI_src "$item"
done


printf 'Jobs Finished!!\n'
exit 0
