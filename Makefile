CC=cmi
COPTS=-O --openmp

all: Grant.o GrantMod.o Massey.o MasseyMod.o

Grant.o: Grant.C Grant.h
	$(CC) $(COPTS) Grant.C

GrantMod.o: GrantMod.C GrantMod.h
	$(CC) $(COPTS) GrantMod.C

Massey.o: Massey.C
	$(CC) $(COPTS) Massey.C

MasseyMod.o: MasseyMod.C
	$(CC) $(COPTS) MasseyMod.C

clean:
	rm -rvf *.o *.so.linux*
