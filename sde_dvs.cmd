;;
;; AC-LGAD Strip Detector (3D)
;;

;;
;; Planar Dimensions
;;
(define WElec 50.0)
(define WChannel @CW@)
(define Pitch @Pitch@)
(define XEdgeOffset 25.0)
(define YEdgeOffset 0.0)
(define WChannelSpc (- Pitch WChannel))
(define nChannels @NCh@)
(define mMinFctr 20.0)
(define mMinFctrZ 6)

;;
;; Pin Electrode Dimensions
;;
;(define WPElec 10.0)
;(define HWPElec (/ WPElec 2.0))
;(define HPElec 0.1) ;; Height of pin electrode
;(define PinPos 5.0) ;; Y offset of pin location

;;
;; Vertical (Z-Axis) Dimensions
;;

; Substrate Thickness
(define Thickness @Thickness@)
; Metal Thickness
(define mThickness 0.50)
; Dielectric Thickness
#if "@Oxide@" == "Default"
(define oxThickness 0.020)
(define niThickness 0.085)
#else
!(
  set thick_str "@Oxide@"
  set thick_num_in_nm [string map {nm ""} $thick_str]
  set thick_num_in_um [expr $thick_num_in_nm*1e-3]
  puts "(define oxThickness ${thick_num_in_um})"
)!
(define niThickness 0.0)
#endif
(define diThickness (+ oxThickness niThickness))
(define mContactThickness (+ diThickness mThickness))
 
;;
;; Materials
;;
(define ContactMat "Aluminum")
(define BulkMat "Silicon")
(define DielecMat "SiO2")
(define DielecTopMat "Si3N4")

;;
;; Calculating the actual device dimensions
;;
; Width: X Axis
(define Wtot (+ (+ (* Pitch nChannels) (* (+ XEdgeOffset WElec) 2.0) ) WChannelSpc ) )
; Length: Y Axis
(define Ltot (+ @Length@ (* YEdgeOffset 2.0) ) )
; Center Location
(define CenterX (/ Wtot 2.0))
(define CenterY (/ Ltot 2.0))
; Updating Parameters in SWB
(define wtot_str (string-append "DOE: DeviceWidth " (number->string Wtot)) )
(define ltot_str (string-append "DOE: DeviceLength " (number->string Ltot)) )
(sde:display-std wtot_str)
(sde:display-std ltot_str)
(define beamlocx_str (string-append "DOE: BeamLocX " (number->string (+ CenterX @BeamOffX@))) )
(define beamlocy_str (string-append "DOE: BeamLocY " (number->string (+ CenterY @BeamOffY@))) )
(sde:display-std beamlocx_str)
(sde:display-std beamlocy_str)

;;
;; Calculating distributed resistance for strips
;;
(define roh_al 2.65e-6)
(define WChcm (* @CW@ 1e-4))
(define LCncm (* Ltot 1e-4))
(define LChcm (* (- @Length@ (* YEdgeOffset 2.0) ) 1e-4) )
(define mThcm (* mThickness 1e-4))
;;(define rdist_al (/ roh_al (* WChcm LChcm) ))
;;(define rdist_al (/ roh_al mThcm ))
;;(define rdist_al_str (string-append "DOE: DResist " (number->string rdist_al) ) )
;;(sde:display-std rdist_al_str)
(define rch_al (* roh_al (/ LChcm (* WChcm mThcm) ) ) )
(define rch_al_str (string-append "DOE: Resist " (number->string rch_al)))
(sde:display-std rch_al_str)

;;
;; Beam Parameters
;;
(define BeamLocationX (+ CenterX @BeamOffX@) )
(define BeamLocationY (+ CenterY @BeamOffY@) )
(define BeamRadius @BeamRadius@)
(define BeamMargin (* BeamRadius 25.0))
(define BeamMarginNarrow (* BeamRadius 2.5))
(define BeamRefStX (- BeamLocationX (/ BeamMargin 2.0) ))
(define BeamRefEndX (+ BeamLocationX (/ BeamMargin 2.0) ))
(define BeamRefNarrowStX (- BeamLocationX (/ BeamMarginNarrow 2.0) ) )
(define BeamRefNarrowEndX (+ BeamLocationX (/ BeamMarginNarrow 2.0) ) )
(define BeamRefStY (- BeamLocationY (/ BeamMargin 2.0) ))
(define BeamRefEndY (+ BeamLocationY (/ BeamMargin 2.0) ))
(define BeamRefNarrowStY (- BeamLocationY (/ BeamMarginNarrow 2.0) ) )
(define BeamRefNarrowEndY (+ BeamLocationY (/ BeamMarginNarrow 2.0) ) )

;;
;; Doping Parameters
;;
; Basic Doping Parameters
(define SubDoping 3e12)
(define ContactDoping 1.0e18) ; Typically 1e18 but can be lowered
(define ContactDopingJTE 1.0e16) ; JTE doping concentration, Typically 1e16
(define ContactDopingSigma 0.05) ; Typically 0.05
(define ContactDopingJTESigma 0.5) ; Typically 1.0
(define ContactDopingMeshDepthMult 3.0)

; nPlus contact layers...
#if "@NPlus@" == "BNL"
(define nPlusFile "@pwd@/reference/doping_n1.dat")
#endif

;;
;; Basic Meshing Parameters
;;
(define aspect_ratio_z (/ Wtot Thickness))
(define aspect_ratio_xy (/ Wtot Ltot))
(define xfctr 4)
(define LWRatio (/ Ltot Wtot))
(if (> LWRatio 1.0)
  (define yfctr (* 4.0 LWRatio) )
  (define yfctr 4.0) )
(define gMeshX (/ Wtot xfctr))
;(define gMeshX (* WChannel 0.8) )

(define gMeshY (/ Ltot yfctr))
(if (> Thickness 50.0)
  (define zfctr 6)
  (define zfctr 4) )
;(define zfctr 10)
(define gMeshZ (/ Thickness zfctr))


;; Top electrode Smoothing
(define TopInnerOffset 5.0)
(define TopOuterOffset 5.0 )
(define gMeshXTop (/ (+ TopInnerOffset TopOuterOffset) 2)) ; Typically 1/2 of |Top Inner + Top Outer|
(define gMeshYTop gMeshY)
(define gMeshXElecTop (/ WElec 4.0))
(define gMeshYElecTop gMeshY)

;; Custom Delaunger
(define CustomDelaunge #f)

;; Misc. Parameters



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Starting the Mesh Creation
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(sde:clear)

;; Defning the basic mesh
; Substrate
(sdegeo:create-cuboid 
  (position 0.0 0.0 0.0)
  (position Wtot Ltot Thickness)
  BulkMat "Substrate")
#if "@Oxide@" == "Default"
; Dielectrics - Default setting (SiOx/SiNx dual layer)
(sdegeo:create-cuboid 
  (position (+ WElec XEdgeOffset) 0.0 (- oxThickness))
  (position (- Wtot (+ WElec XEdgeOffset)) Ltot  0.0)
  DielecMat "OxideLayer")
(sdegeo:create-cuboid 
  (position (+ WElec XEdgeOffset) 0.0 (- diThickness))
  (position (- Wtot (+ WElec XEdgeOffset)) Ltot (- oxThickness))
  DielecTopMat "NitrideLayer")
(sdegeo:create-cuboid 
  (position 0.0 0.0 (- diThickness))
  (position XEdgeOffset Ltot (- oxThickness))
  DielecMat "OxideLeftSpacer")
(sdegeo:create-cuboid 
  (position 0.0 0.0 (- oxThickness))
  (position XEdgeOffset Ltot 0.0)
  DielecTopMat "NitrideLeftSpacer")
(sdegeo:create-cuboid 
  (position (- Wtot XEdgeOffset) 0.0 (- diThickness))
  (position Wtot Ltot (- oxThickness))
  DielecMat "OxideRightSpacer")
(sdegeo:create-cuboid 
  (position (- Wtot XEdgeOffset) 0.0 (- oxThickness))
  (position Wtot Ltot 0.0)
  DielecTopMat "NitrideRightSpacer")
#else
; Dielectrics - Single SiO2 layer
(sdegeo:create-cuboid 
  (position (+ WElec XEdgeOffset) 0.0 (- diThickness))
  (position (- Wtot (+ WElec XEdgeOffset)) Ltot 0.0)
  DielecMat "OxideLayer")
(sdegeo:create-cuboid 
  (position 0.0 0.0 (- diThickness))
  (position XEdgeOffset Ltot 0.0)
  DielecMat "OxideLeftSpacer")
(sdegeo:create-cuboid 
  (position (- Wtot XEdgeOffset) 0.0 (- diThickness))
  (position Wtot Ltot 0.0)
  DielecMat "OxideRightSpacer")
#endif

;; Doping for Substrate
(sdedr:define-refinement-window "Sub.Dop.Win" "Cuboid" 
  (position 0.0 0.0 0.0)
  (position Wtot Ltot Thickness) )
(sdedr:define-constant-profile "Sub.Dop.P" "BoronActiveConcentration" SubDoping)
(sdedr:define-constant-profile-placement "Place.Sub.Dop.P" "Sub.Dop.P" "Sub.Dop.Win")
(sdedr:define-refinement-window "Ref.Win.Substrate" "Cuboid"
  (position 0.0 0.0 0.0)
  (position Wtot Ltot Thickness) )
(sdedr:define-refinement-size "Ref.Win.Substrate"
  gMeshX gMeshY gMeshZ
  (/ gMeshX mMinFctr) (/ gMeshY mMinFctr) (/ gMeshZ mMinFctrZ))
(sdedr:define-refinement-placement "Ref.Win.Substrate" "Ref.Win.Substrate" "Ref.Win.Substrate")

;; Metal Meshes
(define MetalYSt 0.0)
(define MetalYEnd Ltot)
(define MetalYMid (/ (+ MetalYSt MetalYEnd) 2.0))
(define LeftElecStX XEdgeOffset)
(define LeftElecEndX (+ XEdgeOffset WElec))
(define RightElecStX (- Wtot (+ XEdgeOffset WElec)))
(define RightElecEndX (- Wtot XEdgeOffset))
(define mfctr 4.0)
(define vfctr 4.0)

; Mesh refine parameters for Top Contacts
;
; ***************** Most Critical Part, usually *********************
;
(define DopingDepthFctr 5.5)  ; Starting from 5.5
(define ContactRefDepth (* ContactDopingSigma ContactDopingMeshDepthMult))
(define ContactVertFctr 5.5)
(define ContactRefDepthJTE (* ContactDopingJTESigma ContactDopingMeshDepthMult))

(define JTEDepthFctr (* DopingDepthFctr 5.5)) ; Usually larger than 15.0
(if (> JTEDepthFctr Thickness)
  (define ContactJTEVertFctr (* JTEDepthFctr ContactDopingMeshDepthMult))
  (define ContactJTEVertFctr JTEDepthFctr)  )

(define ContactLatFctr 1.0)

; Cathode Contact Profiles
(sdedr:define-gaussian-profile "Impl.TopCn"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" ContactDoping
  "StdDev" ContactDopingSigma
  "Gauss" "Factor" ContactLatFctr)
(sdedr:define-gaussian-profile "Impl.TopCnJTE"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" ContactDopingJTE
  "StdDev" ContactDopingJTESigma
  "Gauss" "Factor" ContactLatFctr)

; L/R Contact Refinement Sizes
(sdedr:define-refinement-size "Ref.Win.TopContacts"
  gMeshXElecTop gMeshY (/ ContactRefDepth ContactVertFctr)
  (/ gMeshXElecTop mfctr) (/ gMeshY mfctr) (/ ContactRefDepth vfctr))

; L/R Contact Corner Refinement Sizes
(sdedr:define-refinement-size "Ref.Win.TopContact.Edges"
  gMeshXTop gMeshY (/ ContactRefDepthJTE ContactJTEVertFctr) 
  (/ gMeshXTop mfctr) (/ gMeshY mfctr)  (/ ContactRefDepthJTE vfctr) )

; Contact metal Doping Concentration Profiles
; Contacts
(sdedr:define-gaussian-profile "Impl.TopCn"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" ContactDoping
  "StdDev" ContactDopingSigma
  "Gauss" "Factor" 1.0)
; JTE
(sdedr:define-gaussian-profile "Impl.TopCnJTE"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" ContactDopingJTE
  "StdDev" ContactDopingJTESigma
  "Gauss" "Factor" ContactLatFctr)

;
; Left Contact Metal
;
(sdegeo:create-cuboid
  (position LeftElecStX MetalYSt (- mContactThickness))
  (position LeftElecEndX MetalYEnd 0.0)
  ContactMat "Metal.TopLeftCn")
;(sdegeo:insert-vertex (position LeftElecStX MetalYMid 0.0))
;(sdegeo:insert-vertex (position LeftElecEndX MetalYMid 0.0))
(sdegeo:define-3d-contact
  (find-face-id 
    (position (+ LeftElecStX (/ WElec 2.0)) MetalYMid 0.0))
  "topleftcn")

; Doping for Left Contact Metal
(sdedr:define-refinement-window "RefWin.TopLeftCn" "Rectangle"
  (position LeftElecStX 0.0 0.0) 
  (position LeftElecEndX Ltot 0.0) )
(sdedr:define-analytical-profile-placement "Impl.TopLeftCn"
  "Impl.TopCn" "RefWin.TopLeftCn"
  "Positive" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "Impl.TopLeftCnJTE"
  "Impl.TopCnJTE" "RefWin.TopLeftCn"
  "Positive" "NoReplace" "Eval")
; Refining for Left Contacts
(sdedr:define-refinement-window "Ref.Win.TopLeft" "Cuboid"
  (position LeftElecStX 0.0 0.0)
  (position (+ LeftElecEndX (/ Pitch 5.0)) Ltot (* ContactRefDepth ContactVertFctr) ) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopLeft"
  "Ref.Win.TopContacts" "Ref.Win.TopLeft")
; Refining for Left Contact Corners  
(sdedr:define-refinement-window "Ref.Win.TopLeft.Left" "Cuboid"
  (position (- LeftElecStX TopOuterOffset) 0.0 0.0)
  (position (+ LeftElecStX TopInnerOffset) Ltot (* ContactRefDepthJTE 2.0)) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopLeft.Left" "Ref.Win.TopContact.Edges" "Ref.Win.TopLeft.Left")
(sdedr:define-refinement-window "Ref.Win.TopLeft.Right" "Cuboid"
  (position (- LeftElecEndX TopInnerOffset) 0.0 0.0)
  (position (+ LeftElecEndX TopOuterOffset) Ltot (* ContactRefDepthJTE 2.0) ) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopLeft.Right" "Ref.Win.TopContact.Edges" "Ref.Win.TopLeft.Right")

;
; Right Contact Metal
;
(sdegeo:create-cuboid
  (position RightElecStX MetalYSt (- mContactThickness))
  (position RightElecEndX MetalYEnd 0.0)
  ContactMat "Metal.TopRightCn")
;(sdegeo:insert-vertex (position RightElecStX MetalYMid 0.0))
;(sdegeo:insert-vertex (position RightElecEndX MetalYMid 0.0))
(sdegeo:define-3d-contact
  (find-face-id 
    (position (+ RightElecStX (/ WElec 2.0)) MetalYMid 0.0) )
  "toprightcn")

; Doping for Right Contact Metal
(sdedr:define-refinement-window "RefWin.TopRightCn" "Rectangle"
  (position RightElecStX 0.0 0.0) 
  (position RightElecEndX Ltot 0.0) )
(sdedr:define-analytical-profile-placement "Impl.TopRightCn"
  "Impl.TopCn" "RefWin.TopRightCn"
  "Positive" "NoReplace" "Eval")
(sdedr:define-analytical-profile-placement "Impl.TopRightCnJTE"
  "Impl.TopCnJTE" "RefWin.TopRightCn"
  "Positive" "NoReplace" "Eval")
; Refining for Right Contacts
(sdedr:define-refinement-window "Ref.Win.TopRight" "Cuboid"
  (position (- RightElecStX (/ Pitch 5.0)) 0.0 0.0)
  (position RightElecEndX Ltot (* ContactRefDepth ContactVertFctr) ) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopRight"
  "Ref.Win.TopContacts" "Ref.Win.TopRight")
; Refining for Right Contact Edges
(sdedr:define-refinement-window "Ref.Win.TopRight.Left" "Cuboid"
  (position (- RightElecStX TopOuterOffset) 0.0 0.0)
  (position (+ RightElecStX TopInnerOffset) Ltot (* ContactRefDepthJTE 2.0) ) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopRight.Left" "Ref.Win.TopContact.Edges" "Ref.Win.TopRight.Left")
(sdedr:define-refinement-window "Ref.Win.TopRight.Right" "Cuboid"
  (position (- RightElecEndX TopInnerOffset) 0.0 0.0 )
  (position (+ RightElecEndX TopOuterOffset) Ltot (* ContactRefDepthJTE 2.0) ) )
(sdedr:define-refinement-placement "Impl.Ref.Win.TopRight.Right" "Ref.Win.TopContact.Edges" "Ref.Win.TopRight.Right")


;
; Back Contact Electrode (Andoe)
;
(sdegeo:create-cuboid
  (position 0.0 0.0 Thickness)
  (position Wtot Ltot (+ Thickness mContactThickness))
  ContactMat "Metal.BackCn")
(sdegeo:define-3d-contact
  (find-face-id
    (position CenterX CenterY Thickness) )
    "backcn")

; Working on Channels
(define CMetalYSt YEdgeOffset)
(define CMetalYEnd (- Ltot YEdgeOffset))
(define CMetalYMid (/ (+ CMetalYSt CMetalYEnd) 2.0))
(define CMetalWidth WChannel)
(define ChCnZEndOffset (- (+ diThickness mThickness)))
(define ChCnZStOffset (- diThickness))

(define ChDepthRefDepth 8.0)
(define ChDepthRefDepthMax (/ ChDepthRefDepth 3.0))
(define ChDepthRefDepthMin (/ ChDepthRefDepthMax 3.0))
(define gMeshYChEdge (/ (- CMetalYEnd CMetalYSt) 1.5) )
(define gMeshXCh (/ CMetalWidth 3.0))
(define ChannelEdge 1.0)

; Channel mesh refinements
(sdedr:define-refinement-size 
  "Ref.Win.Ch.Size"
  gMeshXCh gMeshY (/ ChDepthRefDepthMax 4.0)
  (/ gMeshXCh mMinFctr) (/ gMeshY mMinFctr) (* ChDepthRefDepthMin mMinFctrZ))
; Channel edge mesh refinements
(sdedr:define-refinement-size 
  "Ref.Win.Ch.Edge.Size"
  (/ ChannelEdge 5.0) gMeshY (/ ChDepthRefDepthMax 4.0)
  (/ ChannelEdge mMinFctr) (/ gMeshY mMinFctr) (* ChDepthRefDepthMin mMinFctrZ))



(define MetChStXInit (+ LeftElecEndX (- Pitch WChannel) ) )
!(
  for {set i 0} {$i < @NCh@} {incr i} {
    puts "

(define MetChStX (+ MetChStXInit [expr $i*@Pitch@]) )
(define MetChEndX (+ MetChStX CMetalWidth))

(sdegeo:create-cuboid
  (position MetChStX CMetalYSt ChCnZEndOffset)
  (position (+ MetChStX WChannel) CMetalYEnd ChCnZStOffset)
  ContactMat \"Metal.Ch[expr $i+1]Cn\")
(sdegeo:define-3d-contact
  (find-face-id 
    (position (+ MetChStX (/ CMetalWidth 2.0)) CMetalYMid ChCnZStOffset) )
  \"ch[expr $i+1]cn\")
 
;
; Channel Refinements
;
(sdedr:define-refinement-window \"Ref.Win.Ch${i}\" \"Cuboid\"
  (position (- MetChStX (/ (- Pitch WChannel) 2.0) ) CMetalYSt 0.0)
  (position (+ MetChEndX (/ (- Pitch WChannel) 2.0) ) CMetalYEnd ChDepthRefDepth) )
(sdedr:define-refinement-size 
  \"Ref.Win.Ch${i}.Size\"
  gMeshXCh gMeshY ChDepthRefDepthMax
  (/ gMeshXCh 5.0) (/ gMeshY mMinFctr) ChDepthRefDepthMin)
(sdedr:define-refinement-placement 
  \"Impl.Ref.Win.Ch${i}\" \"Ref.Win.Ch${i}.Size\" \"Ref.Win.Ch${i}\")

;
; Channel Edge Refinements
;
(sdedr:define-refinement-window \"Ref.Win.Ch${i}.Left\" \"Cuboid\"
  (position (- MetChStX ChannelEdge) CMetalYSt 0.0)
  (position (+ MetChStX ChannelEdge) CMetalYEnd ChDepthRefDepth) )
(sdedr:define-refinement-placement
  \"Ref.Win.Ch${i}.Left\" \"Ref.Win.Ch.Edge.Size\" \"Ref.Win.Ch${i}.Left\")
(sdedr:define-refinement-window \"Ref.Win.Ch${i}.Right\" \"Cuboid\"
  (position (- MetChEndX ChannelEdge) CMetalYSt 0.0)
  (position (+ MetChEndX ChannelEdge) CMetalYEnd ChDepthRefDepth) )
(sdedr:define-refinement-placement
  \"Ref.Win.Ch${i}.Right\" \"Ref.Win.Ch.Edge.Size\" \"Ref.Win.Ch${i}.Right\")  

(+ MetChStX Pitch)
"
  }
)!

;;
;; Doping for LGAD
;;
(define LGADStX LeftElecEndX)
(define LGADEndX RightElecStX)
(define LGADStY 0.0)
(define LGADEndY Ltot)
;(define GainLOffset (+ TopOuterOffset 5.0))
;(define GainLOffset 1.0)
;(define GainLOffset (/ WElec 5.0))
(define GainLOffset (/ Pitch 10.0))


; Placement window for LGAD n+
(sdedr:define-refinement-window "RefWin.LGADCn" "Rectangle"
  (position LGADStX LGADStY 0.0)
  (position LGADEndX LGADEndY 0.0) )
; Placement window for Gain Layer
(sdedr:define-refinement-window "RefWin.LGADGain" "Rectangle"
  (position (+ LGADStX GainLOffset) LGADStY 0.0)
  (position (- LGADEndX GainLOffset) LGADEndY 0.0) )

;
; n+ Layer
;
; BNL n+ Profile
(define nPlusFile "@pwd@/reference/doping_n1.dat")

; Selecting analytical profiles

; Deep BNL profile (+1 um deeper)
#if "@GainLayer@" == "BNLDeep"

#if "@NPlus@" == "400"
(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 2.4074534608957005e+18 "StdDev" 0.22411056126365628
   "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlus"
  "LGADnPlus" "RefWin.LGADCn"
  "Positive" "NoReplace" "Eval")

#elif "@NPlus@" == "1600"
(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 1.7500921207452422e+17 "StdDev" 0.25
   "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlus"
  "LGADnPlus" "RefWin.LGADCn"
  "Positive" "NoReplace" "Eval")
#endif

#endif

; Regular BNL gain profile...
#if "@GainLayer@" == "BNL"
#if "@NPlus@" == "2k"

(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 1.4e+17 "StdDev" 0.25
  "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlusLGAD"
  "LGADnPlus" "RefWin.LGADCn"
  "Both" "NoReplace" "Eval")

#elif "@NPlus@" == "1k"

(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 9.771301071647982e+17 "StdDev" 0.15
  "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlusLGAD"
  "LGADnPlus" "RefWin.LGADCn"
  "Both" "NoReplace" "Eval")

#elif "@NPlus@" == "400"

(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 2.1901224176217385e+18 "StdDev" 0.25
   "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlus"
  "LGADnPlus" "RefWin.LGADCn"
  "Positive" "NoReplace" "Eval")

#elif "@NPlus@" == "1600"

(sdedr:define-gaussian-profile "LGADnPlus"
  "PhosphorusActiveConcentration"
  "PeakPos" 0.0 "PeakVal" 2.0447980334234858e+17 "StdDev" 0.24257004905553878
   "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlus"
  "LGADnPlus" "RefWin.LGADCn"
  "Positive" "NoReplace" "Eval")

#elif "@NPlus@" == "BNL"

(sdedr:define-1d-external-profile "LGADnPlus" nPlusFile 
  "Scale" 1.0 "Range" 0 @Thickness@ "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.nPlusLGAD"
  "LGADnPlus" "RefWin.LGADCn"
  "Positive" "NoReplace" "Eval")

#endif

#endif

;
; Gain Layer
;
;; Amplification (gain) layer doping profiles from file.
#if "@GainLayer@" == "BNL"
(define pPlusFile "@pwd@/reference/doping_p1.dat")
#endif
#if "@GainLayer@" == "FBK"
(define pPlusFile "@pwd@/reference/doping_pFBK.dat")
#endif
#if "@GainLayer@" == "BNLDeep"
(define pPlusFile "@pwd@/reference/doping_p1_deep.dat")
#endif 

#if "@GainLayer@" != "NoAmp"
(sdedr:define-1d-external-profile "LGADAmp" pPlusFile 
  "Scale" 1.0 "Range" 0.0 Thickness "Gauss" "Factor" 1.0)
(sdedr:define-analytical-profile-placement "Impl.pPlusFile"
  "LGADAmp" "RefWin.LGADGain"
  "Positive" "NoReplace" "Eval")
#endif

;
; Refining LGAD Area
;
(define LGADDepth 1.25) ; Usually 1.25
(define LGADDepthFctr DopingDepthFctr)
(define LGADLenX (- LGADEndX LGADStX))
(define LGADLenY Ltot)
(define gLGADX (/ Pitch 4.0) )
(define gLGADY (/ Ltot yfctr) )
(sdedr:define-refinement-window "Ref.Win.LGAD.Contact" "Cuboid"
  (position LGADStX LGADStY 0.0)
  (position LGADEndX LGADEndY LGADDepth) )
(sdedr:define-refinement-size "Ref.Win.LGAD.Contact"
  gLGADX gLGADY (/ LGADDepth LGADDepthFctr)
  (/ gLGADX mMinFctr) (/ gLGADY mMinFctr) (/ LGADDepth (* LGADDepthFctr mMinFctrZ) ) )
(sdedr:define-refinement-placement 
  "Ref.Win.LGAD.Contact" "Ref.Win.LGAD.Contact" "Ref.Win.LGAD.Contact")

;;
;; Beam Refinement
;;

;; Vicinity of Beam
(define BeamLatRefMargin 50.0)
;(define BeamLatRefMargin (/ Pitch 8.0))
;(define BeamLatRefStX (- BeamRefStX BeamLatRefMargin))
;(define BeamLatRefEndX (+ BeamRefEndX BeamLatRefMargin))
(define BeamLatRefStX 0)
(define BeamLatRefEndX Wtot)
(define BeamLatRefStY (- BeamRefStY BeamLatRefMargin))
(define BeamLatRefEndY (+ BeamRefEndY BeamLatRefMargin))
(define BeamLatRefEndZ (* LGADDepth 1.0))
(sdedr:define-refinement-window "Ref.Win.Optical.Surf" "Cuboid"
  (position BeamLatRefStX BeamLatRefStY 0.0) 
  (position BeamLatRefEndX BeamLatRefEndY BeamLatRefEndZ) )
(sdedr:define-refinement-size "Ref.Win.Optical.Surf.Size"
  (/ BeamLatRefMargin 1.25) (/ BeamLatRefMargin 1.25) (/ BeamLatRefEndZ 1.25)
  (/ BeamLatRefMargin 2.5) (/ BeamLatRefMargin 2.5) (/ BeamLatRefEndZ 2.5))
;(sdedr:define-refinement-size "Ref.Win.Optical"
; (* BeamRadius 5.0) (* BeamRadius 2.0) (/ BeamLatRefEndZ 1.0)
; (* BeamRadius 1.0) (* BeamRadius 1.0) (/ BeamLatRefEndZ LGADDepthFctr))

(sdedr:define-refinement-placement "Impl.Ref.Win.Optical.Surf" "Ref.Win.Optical.Surf.Size" "Ref.Win.Optical.Surf")

;; The Beam itself
(sdedr:define-refinement-window "Ref.Win.Optical" "Cuboid"
  (position BeamRefStX BeamRefStY 0.0) 
  (position BeamRefEndX BeamRefEndY Thickness) )
(sdedr:define-refinement-size "Ref.Win.Optical.Size"
  BeamRadius BeamRadius gMeshZ
  (/ BeamRadius 3.0) (/ BeamRadius 3.0) (/ gMeshZ 3.0))
(sdedr:define-refinement-placement "Impl.Ref.Win.Optical" "Ref.Win.Optical.Size" "Ref.Win.Optical")



;;
;; Relaxing Mesh at the bottom
;;
(define BackCnDepth (- Thickness 1.0))
(define BackCnEnd Thickness)
(sdedr:define-refinement-window "Ref.Win.Back.Contact" "Cuboid"
  (position 0.0 0.0 BackCnDepth)
  (position Wtot Ltot BackCnEnd) )
(sdedr:define-refinement-size "Ref.Win.Back.Contact"
  gMeshX gMeshY gMeshZ
  (/ gMeshX 3.0) (/ gMeshY 3.0) (/ gMeshZ 3.0) )
(sdedr:define-refinement-placement "Ref.Win.Back.Contact" "Ref.Win.Back.Contact" "Ref.Win.Back.Contact")


;
; Finally, Doping dependent meshing.
;
(sdedr:define-refinement-function "Ref.Win.Substrate" "DopingConcentration" "MaxTransDiff" 1.25)




;;
;; Setting up Delaunizer
;;
(if (eq? CustomDelaunge #t)
  (sdedelaunizer:set-parameters 
    "type" "boxmethod" "maxPoints" 100000
    "coplanarityDistance" 1 
    "edgeProximity" 0.1 
    "faceProximity" 0.1
    "sliverAngle" 170
    "sliverDistance" 0.01 
    "maxSolidAngle" 170
    "maxNeighborRatio" 1e+6 
    "coplanarityAngle" 179
    "minEdgeLength" 1e-2
    "delaunayTolerance" 1
    "storeDelaunayWeight" #t) )

;;----------------------------------------------------------------------
;; Build Mesh 
;; Implemented CPU number detection script.
!(
  proc numberOfCPUs {} {
    if {![catch {open "/proc/cpuinfo"} f]} {
      set cores [regexp -all -line {^processor\s} [read $f]]
      close $f
      if {$cores > 0} {
        return $cores
      }
    }
  }
  # proc numberOfCPUs
  
  set numProcs [ numberOfCPUs ]
  puts "(sde:build-mesh \"snmesh\" \"-numThreads ${numProcs} -AI -c boxmethod\" \"n@node@_msh\")"
)!
