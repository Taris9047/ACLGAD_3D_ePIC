1. No avalanche contact.
2. No backplate doping.
3. All electrodes are ground except the backplate.
4. Y profile of contact electrodes: sigma*sqrt(2)=1.0
5. None of doping profiles has lateral spread. Just zero!
6. srh and fldmob
7. Metal thickness: 50 nm

Parameters on the pdf/pptx file

Bulk = 50 um
Pitch= 200 um
Metal = 80 um
No. of channels = 7
SiO2 thickness= 20 nm
Si3N4 thickness= 85 nm
N+ termination width= 50 um