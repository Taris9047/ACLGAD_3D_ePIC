#!/usr/bin/env python
# coding: utf-8

# In[1]:


# PCUNITS keyword conversion.
#
# Charge Density to Density conversion in VictoryDevice's single event excitation.
#

import numpy as np

# Given Density
D = 1.28e-5 * 1e-12 * 1e4 # C/cm
Radius = 0.1*1e-4 # in cm
q = 1.60218e-19 # C

# Sensor thickness
Thickness = 50 # in um

# Exposure time (sec)
Texp = 10e-12

# Beam density (ehp generation / cm3 / sec)
#BD = D / (q*np.pi*(Radius**2.0)) / Texp

# Beam density again.. from 80 ehp/um MPV --> For this, beam width needs to be negligible. 1 nm scale.
BD = (80*1e4)**3.0/Texp

print("Beam density in ehp generation per cubic cm term per second\n{} ehp/cm3/sec".format(BD))
print("{0:.6E} ehp/cm3/sec".format(BD))


# In[2]:


# Writing Excitation profile.
outf_name='Excitation.plx'
data_text='# Optical generation for ACLGAD\n\"OpticalGeneration\"\n'

depth_space = np.linspace(0.0, Thickness, 6)
for de in depth_space:
    data_text += "{} {}\n".format(de, BD)

with open(outf_name, 'w') as fp:
    fp.write(data_text)
    print("{} written!".format(outf_name))

print("\nThe data are...\n")
print(data_text)


# In[ ]:




