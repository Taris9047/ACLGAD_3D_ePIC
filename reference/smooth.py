#!/usr/bin/env -m python

import os
import sys
import numpy as np
import math


in_file = 'doping_n1.dat'

# Reading in file
if len(sys.argv) >= 2:
    in_file = os.path.realpath(sys.argv[1])

outf_name = in_file + '_smoothed'

data = {'x':[], 'y':[]}
headers = []
with open(in_file, 'r') as fp:
    lines = [ _.strip() for _ in fp.readlines() ]
    for l in lines:
        if not l:
            continue

        if l[0] == '#' or l[0] == '\"':
            headers.append(l)
            continue
        
        data_split = l.split()
        data['x'].append(float(data_split[0]))
        data['y'].append(float(data_split[1]))

# Smooth function
# Reference:
# https://stackoverflow.com/questions/20618804/how-to-smooth-a-curve-in-the-right-way
def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

def fexp(f):
    return int(math.floor(math.log10(abs(f)))) if f != 0 else 0

def round_big(x, dec=3):
    pow_of = fexp(x) - dec
    return int(x/(10**pow_of))*(10**pow_of)

# Smoothng data
x = np.array([round(_,2) for _ in data['x']])
y = np.array(data['y'])
y_smooth = [ float(round_big(_, 2)) for _ in smooth(y, 50) ]

smoothed_data = {'x':x, 'y':y_smooth}

# Writing file...
with open(outf_name, 'w') as fp:
    for h in headers:
        fp.write(h+os.linesep)
    
    for x_, y_ in zip(smoothed_data['x'], smoothed_data['y']):
        fp.write('{}\t{}{}'.format(str(x_), str(y_), os.linesep))

    fp.write(os.linesep)

    print('Written {}...'.format(outf_name))