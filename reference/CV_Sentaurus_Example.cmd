
*----------------------------------------------------------------------
Device LDMOS{
*----------------------------------------------------------------------
File {
   * input files:
   Grid=   "@tdr@"
   Parameters= "@parameter@"
   * output files:
   Plot=   "@tdrdat@"
   Current="@plot@"
}

Electrode {
   { Name="source"    Voltage= 0.0  }
   { Name="drain"     Voltage= 0.0  }
   { Name="gate"      Voltage= 0.0 }
   { Name="substrate" Voltage= 0.0 }
}

Physics{  
   Mobility(
      DopingDependence 
      HighFieldSaturation
      Enormal
   )
   Recombination(
      SRH( DopingDependence TempDependence )
      Auger
    )        
}

*----------------------------------------------------------------------
} * End of Device
*----------------------------------------------------------------------

File {
   Output= "@log@"
   ACExtract= "@acplot@"
}

Math {
	Extrapolate
	Notdamped= 50
	Iterations= 15
	ExitOnFailure
	eMobilityAveraging= ElementEdge
	* uses edge mobility instead of element one for electron mobility	
	hMobilityAveraging= ElementEdge   
	* uses edge mobility instead of element one for hole mobility
	GeometricDistances 		         
	* when needed, compute distance to the interface instead of closest point on the interface
	ParameterInheritance= Flatten 	  
	* regions inherit parameters from materials  
}
Plot{
*--Density and Currents, etc
   eDensity hDensity
   TotalCurrent/Vector eCurrent/Vector hCurrent/Vector
   eMobility hMobility
   eVelocity hVelocity
   eQuasiFermi hQuasiFermi

*--Temperature 
   eTemperature Temperature hTemperature

*--Fields and charges
   ElectricField/Vector Potential SpaceCharge

*--Doping Profiles
   Doping DonorConcentration AcceptorConcentration

*--Generation/Recombination
   SRH Band2Band * Auger
   * AvalancheGeneration eAvalancheGeneration hAvalancheGeneration

*--Driving forces
   eGradQuasiFermi/Vector hGradQuasiFermi/Vector
   eEparallel hEparallel eENormal hENormal

*--Band structure/Composition
   BandGap 
   BandGapNarrowing
   Affinity
   ConductionBand ValenceBand
   eQuantumPotential hQuantumPotential

*--Gate Tunneling
   * eBarrierTunneling hBarrierTunneling  BarrierTunneling
   * eDirectTunnel hDirectTunnel
}
System { 
  LDMOS ldmos (drain=d source=s gate=g substrate=b) 

  Vsource_pset vd ( d 0 ){ dc= 0 } 
  Vsource_pset vs ( s 0 ){ dc= 0 } 
  Vsource_pset vg ( g 0 ){ dc= 0 } 
  Vsource_pset vb ( b 0 ){ dc= 0 }
} 



Solve {
#- Creating initial guess:
   Coupled(Iterations= 100 LineSearchDamping= 1e-4){ Poisson } 
   Coupled{ Poisson Electron Hole } 

   Quasistationary( 
      InitialStep= 1e-2 Increment= 1.35 
      MinStep= 1e-5 MaxStep= 0.1 
      Goal { Parameter= vd.dc Voltage= 0.0 } 
   ){ Coupled { Poisson  Electron Hole } }

#- Vb sweep for Vd=0.0
   NewCurrentFile="" 
   Quasistationary( 
      DoZero 
      InitialStep= 1e-2 Increment= 1.35 
      MinStep= 1e-5 MaxStep= 0.04
      Goal { Parameter= vd.dc Voltage= 10.0} 
   ){ ACCoupled ( 
       StartFrequency= 1e6 EndFrequency= 1e6 NumberOfPoints= 1 Decade 
       Node(d s g b) Exclude(vd vs vg vb) 
       ACCompute (Time= (Range= (0 1)  Intervals= 30)) 
      ){ Poisson Electron Hole } 
   }
}

