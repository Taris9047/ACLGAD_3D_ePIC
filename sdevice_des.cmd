*- Simulation on Multi Channel ACLGAD -*

#setdep @node|sde@

#define TOn @<0.1*1e-12>@
#define TOnStart @<1e-10>@
#define TOnEnd @<TOnStart+TOn>@
#define TOnSigma @<TOn/100.0>@

#define TrMaxIntervals @<300>@
#define TrIntervalsBeforeEx @<25>@
#define TrIntervalsEx @<50>@
#define TrIntervals @<TrMaxIntervals-TrIntervalsBeforeEx-TrIntervalsEx>@

#define TrPeak @<3e-9>@
#define TrIntervalsBfPeak @<100>@
#define TrIntervalsAfPeak @<TrIntervals-TrIntervalsBfPeak>@

#define TSim @<10e-9>@
#define TSimMaxStep @<TSim/TrIntervals>@

#define GRatio @<1.61803398875>@
#define DecrRatio @<GRatio>@

*- Beam stuffs
#define BeamLocX @BeamLocX@
#define BeamLocY @BeamLocY@

*- Bias condition
#define VBias @<@BackBias@>@
#define VPrelimBias @<@BackBias@*0.1>@
#define VBiasStep @<-VBias/2000.0>@
#define VPrelimBiasStep @<-VPrelimBias/10.0>@

*- Ambient Temperature
#define AmbientTemp @<298.0>@

*- Trap density
#define SiOxTrapDensity 2.3e10

*- Sim Break Value
#define BreakCurrent @<1e-6*@Length@>@

*- Running CMI compile
!(
#exec @pwd@/compile_PMI.sh
exec make clean
exec make all
)!



File {
  Grid= "@tdr@"
  Plot= "@tdrdat@"
  Current= "@plot@"
  Output= "@log@"
  Parameter= "@pwd@/sdevice_Al.par"
  
  *- PMI Path
  PMIPath = "@pwd@/"
}


Electrode {
  { Name="topleftcn" Voltage=0.0 Material="Aluminum" }
  { Name="toprightcn" Voltage=0.0 Material="Aluminum" }
  { Name="backcn" Voltage=0.0 Material="Aluminum" }
!(
  for {set i 1} {$i <= @NCh@} {incr i} {
  	puts "  { Name=\"ch[expr $i]cn\" Voltage=0.0 Material=\"Aluminum\" }"
  }
)!

} *- Electrode


*- Physical model definition for FastDiamond material
Physics (Material = "Silicon") {

  Fermi

  EffectiveIntrinsicDensity(NoBandGapNarrowing)
  *- EffectiveIntrinsicDensity( BandGapNarrowing(SlotBoom) )

  Recombination (
	SRH(
	  DopingDependence
	  TempDependence
	  ElectricField (Lifetime=Schenk Densitycorrection=local)
	  *- Tunneling(Hurkx)
	) *- SRH
	SurfaceSRH
	Auger
	Band2Band

#if [ string compare @ImpactModel@ "Grant" ] == 0
	*- PMI Model
	Avalanche (Grant)
#endif
#if [ string compare @ImpactModel@ "GrantMod" ] == 0
	*- PMI Model
	Avalanche (GrantMod)
#endif
#if [ string compare @ImpactModel@ "Vanovers" ] == 0
	Avalanche (vanOverstraetendeMan)
#endif
#if [ string compare @ImpactModel@ "Massey" ] == 0
	Avalanche (Massey)
#endif
#if [ string compare @ImpactModel@ "MasseyMod" ] == 0
  Avalanche (MasseyMod)
#endif
#if [ string compare @ImpactModel@ "Okuto" ] == 0
	Avalanche (Okuto)
#endif
  )

  Mobility (
	DopingDependence
	HighFieldSaturation
	*- Enormal( Lombardi )
  )

  Temperature = AmbientTemp
  
#if [string compare @SIM@ "Breakdown"] != 0
*- Physics for optical generation
  HeavyIon (
	Direction=(0,0,1)
	Location=(BeamLocX,BeamLocY,0)
	Time=TOnStart
	Length=@Thickness@
	*- LET_f=@<(@BDensity@*1e4)*(@BDensity@*1e4)*(@BDensity@*1e4)>@
	LET_f=@BDensity@
!(
  puts "    Wt_hi=@BeamRadius@"
)!
	Exponential
	PicoCoulomb
  ) *- HeavyIon
#endif
  
} * Physics of Silicon


*- Trap
*-Physics(MaterialInterface="Silicon/SiO2") {
*-  Traps((FixedCharge Conc=SiOxTrapDensity))
*-}



  



*- Parameters to collect.
Plot {
  ConductionBand Valenceband
  EffectiveBandGap BandGap
  eDensity hDensity
  eCurrent hCurrent
  eCurrent/Vector hCurrent/Vector Current/Vector
  ConductionCurrent/Vector DisplacementCurrent/Vector
  ElectricField
  ElectricField/Vector
  eQuasiFermi hQuasiFermi
  eGradQuasiFermi/Vector hGradQuasiFermi/Vector
  egradQuasiFermi hgradQuasiFermi
  Potential Doping SpaceCharge
  SRH Auger
  AvalancheGeneration
  eAvalanche hAvalanche
  eMobility hMobility
  DonorConcentration AcceptorConcentration
  Doping
  eVelocity hVelocity
  ConductionBandEnergy ValenceBandEnergy BandGap
  SpaceCharge
  
  eTrappedCharge hTrappedCharge
  eGapStatesRecombination hGapStatesRecombination 

#if [string compare @SIM@ "Breakdown"] != 0
  HeavyIonChargeDensity
#endif
}

*- Defining parameters to be written in the transient responses.

CurrentPlot {
  
  AvalancheGeneration(
	Integrate(Material="Silicon")
  )

#if [string compare @SIM@ "Breakdown"] != 0
  HeavyIonGeneration (
	Integrate(Material="Silicon")
  )
#endif

}



*- Computation setting.
Math {
  Extrapolate
  IncompleteNewton
  RelErrcontrol
  NumberOfThreads = Maximum
  Number_of_Solver_Threads = Maximum
  RHSmax = 1e32
  RHSFactor = 1e32
  Iterations = 24
  Digits = 5
  Method = ILS(set=5)
  SubMethod = ParDiSo
  ILSrc="set (5) {
      iterative(
        gmres(100), tolrel=1e-9, tolunprec=1e-4, tolabs=0, maxit=250);
        preconditioning(ilut(1e-7,-1), left);
        ordering(symmetric=nd, nonsymmetric=mpsilst);
        options(compact=yes, linscale=0, refineresidual=26, refineiterate=1, refinebasis=1, verbose=1); };"
  Transient = BE
  *- Transient = TRBDF
  *- ExtendedPrecision
  ExitOnFailure
  Wallclock
  
  NoSRHperPotential *- Field dependent SRH modifier.
}




*- Solving for transient response of optical generation.
Solve
{
  *- Ramping up the bias.
  NewCurrentFile = "IV_"
  
  *- Obtaining initial conditions
  Coupled(Iterations=800) { Poisson }
  Coupled(Iterations=400) { Poisson Electron Hole } 
  
  Quasistationary (
    Initialstep = 1e-4
    Increment = 1.05 Decrement = DecrRatio
    Maxstep = VBiasStep Minstep = 1.e-9
    Goal { Name="backcn" Voltage = VBias }
    BreakCriteria {
      Current(
        Contact="backcn" Absval=BreakCurrent)
    }
  ) { Coupled { Poisson Electron Hole }
  	*- Plot(FilePrefix="IV_ramp")
  }
  Save(FilePrefix="IV_ramp_end")
  
#if [string compare @SIM@ "Breakdown"] != 0
  *- Load(FilePrefix="IV_ramp_end")
  *- Coupled( Iterations=400) { Poisson Electron Hole }
  
  NewCurrentFile = "Tran_Photo_"
  Transient (
    InitialTime = 0.0
    FinalTime = TSim
    Initialstep = 1e-13
    Minstep = 1e-24
    Maxstep = TSimMaxStep
    Increment = GRatio Decrement = DecrRatio
  ) { Coupled { Poisson Electron Hole }
      Plot(
        *- NoOverwrite
        Time=(
          range = (0 TOnStart) intervals = TrIntervalsBeforeEx;
          range = (TOnStart TOnEnd) intervals = TrIntervalsEx;
          range = (TOnEnd TrPeak) intervals = TrIntervalsBfPeak;
          range = (TrPeak TSim) intervals = TrIntervalsAfPeak
        )
        *- FilePrefix="Tran_Anim_"
      ) * Plot
    }
  Save(FilePrefix = "Photo_On_Last_")
#endif
}
