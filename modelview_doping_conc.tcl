#/home/taris/Data/ACLGAD_3D/modelview_doping_conc.tcl
#MAKO: Output from Tcl commands history.
load_script_file /home/taris/Data/ACLGAD_3D/n40_vis.tcl
#-> 0
create_cutplane -plot Plot_n36_msh -type y -at 468.075
#-> C1(n36_msh)
create_plot -dataset C1(n36_msh)
select_plots Plot_C1(n36_msh)
#-> Plot_C1(n36_msh)
#-> Plot_C1(n36_msh)
create_cutline -plot Plot_C1(n36_msh) -type x -at 2095.2
#-> C1(C1(n36_msh))
create_plot -dataset C1(C1(n36_msh)) -1d
select_plots Plot_C1(C1(n36_msh))
#-> Plot_C1(C1(n36_msh))
#-> Plot_C1(C1(n36_msh))
create_curve -plot Plot_C1(C1(n36_msh)) -dataset C1(C1(n36_msh)) -axisX Z -axisY DopingConcentration
#-> Curve_1
set_axis_prop -plot Plot_C1(C1(n36_msh)) -axis y -type log
#-> 0
zoom_plot -plot Plot_C1(C1(n36_msh)) -window {-2.67958 9.77465e+10 7.30358 1.18707e+19}
set_legend_prop -plot Plot_C1(C1(n36_msh)) -position {0.228395 0.192926}
#-> 0
set_curve_prop -plot Plot_C1(C1(n36_msh)) Curve_1 -show_markers
#-> 0

