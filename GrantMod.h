/********************************
 *
 * PMI implementation of Grant impact ionization model
 *
 * Written by Taylor Shin
 *
 */

#ifndef _PMI_GRANT_MOD_MODEL_H_
#define _PMI_GRANT_MOD_MODEL_H_
 
#include <cmath>

#include "PMI.h"

/*
class PMI_Avalanche : public PMI_Vertex_Interface {

private:
  const PMI_AvalancheDrivingForce drivingForce;
  
public:

  PMI_Avalanche (const PMI_Environment& env,
    const PMI_AvalancheDrivingForce force);
  virtual ~PMI_Avalanche();
  
  PMI_AvalancheDrivingForce AvalancheDrivingForce () const
  { return drivingForce; }
  
  virtual void Compute_alpha (const double F) = 0;
};

typedef PMI_Avalanche* new_PMI_Avalanche_func(
  const PMI_Environment& env, const PMI_AvalancheDrivingForce force);
extern "C" 
new_PMI_Avalanche_func new_PMI_e_Avalanche;
extern "C" 
new_PMI_Avalanche_func new_PMI_h_Avalanche;
*/

#define GrantMod_Param_Modifier_A 1.0e-2 /* Orders of magnitude smaller? */
#define GrantMod_Param_Modifier_B 1.0

#define GrantMod_Param_f_high 5.3e+7 /* Default: 5.3e+5 */
#define GrantMod_Param_f_low  2.4e+6 /* Default: 2.4e+5 */

#endif /* Include guard */
