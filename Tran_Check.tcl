#/home/taris/Data/Projects/ACLGAD_3D/Tran_Check.tcl
#MAKO: Output from Tcl commands history.
load_file /home/taris/Data/Projects/ACLGAD_3D/Tran_Photo_n380_des.plt
create_plot -1d
select_plots Plot_1
#-> Plot_1
#-> Plot_1
#-> Tran_Photo_n380_des
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch4Cn Charge}
#-> Curve_1
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY2 {IntegrSilicon HeavyIonGeneration}
#-> 0
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch5Cn Charge}
#-> Curve_3
set_curve_prop -plot Plot_1 Curve_1 -deriv 1
#-> 0
set_curve_prop -plot Plot_1 Curve_2 -deriv 1
#-> 0
zoom_plot -plot Plot_1 -reset
set_axis_prop -plot Plot_1 -axis y -type log
#-> 0
set_axis_prop -plot Plot_1 -axis y -type linear
#-> 0
set_curve_prop -plot Plot_1 Curve_3 -deriv 1
#-> 0
set_curve_prop -plot Plot_1 Curve_2 -deriv 0
#-> 0
remove_curves -plot Plot_1 Curve_3
#-> Curve_3
remove_curves -plot Plot_1 Curve_1
#-> Curve_1
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch4Cn TotalCurrent}
remove_curves -plot Plot_1 Curve_3
#-> Curve_3
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch4Cn Charge}
#-> Curve_3
set_curve_prop -plot Plot_1 Curve_3 -deriv 1
#-> 0
zoom_plot -plot Plot_1 -reset
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch5Cn Charge}
#-> Curve_4
set_curve_prop -plot Plot_1 Curve_4 -deriv 1
#-> 0
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch6Cn Charge}
#-> Curve_5
set_curve_prop -plot Plot_1 Curve_5 -deriv 1
#-> 0
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch7Cn Charge}
#-> Curve_6
set_curve_prop -plot Plot_1 Curve_6 -deriv 1
#-> 0
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch1Cn Charge}
#-> Curve_7
set_curve_prop -plot Plot_1 Curve_7 -deriv 1
#-> 0
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch2Cn Charge}
#-> Curve_8
create_curve -plot Plot_1 -dataset Tran_Photo_n380_des -axisX time -axisY {Ch3Cn Charge}
#-> Curve_9
set_curve_prop -plot Plot_1 Curve_8 -deriv 1
#-> 0
set_curve_prop -plot Plot_1 Curve_9 -deriv 1
#-> 0
set_axis_prop -plot Plot_1 -axis y2 -type log
#-> 0
zoom_plot -plot Plot_1 -reset
set_legend_prop -plot Plot_1 -position {0.218182 0.903846}
#-> 0
set_axis_prop -plot Plot_1 -axis y -type log
#-> 0
set_axis_prop -plot Plot_1 -axis y2 -type linear
#-> 0
zoom_plot -plot Plot_1 -reset
set_axis_prop -plot Plot_1 -axis y -type linear
#-> 0
zoom_plot -plot Plot_1 -reset
set_legend_prop -plot Plot_1 -position {0.0278075 0.988166}
#-> 0

